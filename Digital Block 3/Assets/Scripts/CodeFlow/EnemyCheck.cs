﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCheck : MonoBehaviour
{
    public int enemyDistance = 0;
    public int enemyCount = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("space"))
        {
            //EnemySearch();
            //DestroyEnemy();
            EnemyScan();    
        }

    }

    void EnemySearch()
    {
        for(int i = 0; i < 5; i++)
        {
            enemyDistance = Random.Range(1, 100);
            
            if(enemyDistance >= 8)
            {
                print("¡Hay un enemigo lejos!");
            }
            if (enemyDistance >= 4 && enemyDistance <= 7)
            {
                print("¡Hay un enemigo a media distancia!");
            }
            if (enemyDistance < 4)
            {
                print("¡Hay un enemigo muy cerca!");
            }

        }
    }

    void DestroyEnemy()
    {
        while(enemyCount > 0)
        {
            print("Hay un enemigo! Destruyamoslo");
            enemyCount--;
        }
    }

    void EnemyScan()
    {
        bool isAlive = false;

        do
        {
            print("Buscando enemigos...");
        }
        while (isAlive == true);
    }


}
