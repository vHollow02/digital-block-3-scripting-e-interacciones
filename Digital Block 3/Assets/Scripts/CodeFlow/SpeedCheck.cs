﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedCheck : MonoBehaviour
{
    public float speed = 0f;
    public float distance = 0f;
    public float time = 0f;

    public float maxSpeed = 70f;
    public float minSpeed = 40f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("space"))
        {
            Check();
        }
    }

    void Check()
    {
        speed = distance / time;
        
        if (speed > maxSpeed)
        {
            print("¡Estas yendo demasiado rápido!");
        }
        else if(speed < minSpeed)
        {
            print("¡Estas yendo demasiado despacio!");
        }
        else if(speed == maxSpeed || speed == minSpeed)
        {
            print("Estas en el límite de velocidad ¡Cuidado!");
        }
        else
        {
            print("llevas una velocidad correcta");
        }

        print("vas a " + speed + " km/h");
    }
        

}
