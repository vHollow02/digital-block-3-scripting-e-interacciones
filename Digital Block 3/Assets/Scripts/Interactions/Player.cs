﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public CapsuleCollider playerCollider;
    public float moveSpeed;

    public GameObject door;

    private GameObject enemy;
    private Enemy enemyScript;

    private Ray ray;
    public float rayDistance = 4f;

    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GetComponent<CapsuleCollider>();

        playerCollider.height = 1f;
        playerCollider.center = new Vector3(0f, 0.5f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        transform.Translate(movement * Time.deltaTime * moveSpeed);

        ray = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);

        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            if(hit.collider.gameObject.tag == "Enemy")
            {
                print("¡Estas mirando a un enemigo!");               
            }

            if (hit.collider.gameObject.tag == "Chest")
            {
                hit.collider.gameObject.GetComponent<TreasureChest>().interactable = true;
            }
            
        }


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            
                enemyScript = collision.gameObject.GetComponent<Enemy>();
                enemyScript.enemyHealth--;            
        }

        if (collision.gameObject.tag == "DoorLever")
        {            
                Destroy(door);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Elevator")
        {
            transform.position = new Vector3(-4.3f, 3.6f,-4.5f);
        }
    }


}

